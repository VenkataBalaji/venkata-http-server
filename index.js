const http = require("http");
const fs = require("fs").promises;
const uuid = require("uuid");
const json = require("./index.json");

//create http server
let server = http.createServer((req, res) => {
  //handle GET request for "/html"
  if (req.method === "GET" && req.url === "/html") {
    res.writeHead(200, { "Content-Type": "text/html" });
    //read index,html file asynchrously
    return fs
      .readFile("index.html", "utf-8")
      .then((data) => {
        res.write(data);
        res.end();
      })
      .catch((error) => {
        console.log(error);
        res.writeHead(500, { "Content-Type": "text/plain" });
        res.end(" Server Error"); //send server error meassage
      });
  } else if (req.method === "GET" && req.url === "/json") {
    //handle GET request for "/json"
    res.writeHead(200, { "Content-Type": "text/json" });
    try {
      //console.log(data)
      res.write(JSON.stringify(json)); //write JSON data to response
      res.end();
    } catch (error) {
      console.log(error);
      res.writeHead(500, { "Content-Type": "text/plain" });
      res.end(" Server Error");
    }
  } else if (req.method === "GET" && req.url === "/uuid") {
    //handle GET request for "/uuid"
    try {
      //Create an object to hold data
      let data = {};
      data["uuid"] = uuid.v4();
      res.write(JSON.stringify(data));
      res.end();
    } catch (error) {
      console.log(error);
      res.writeHead(500, { "Content-Type": "text/plain" });
      res.end(" Server Error");
    }
  } else if (
    req.method === "GET" &&
    req.url.startsWith("/status/") &&
    !isNaN(Number(req.url.split("/")[2]))
  ) {
    // Handle GET request for "/status/{status_code}"
    try {
      let statusCode = Number(req.url.split("/")[2]);
      res.writeHead(statusCode, { "Content-Type": "text/plain" });
      res.write(`Response with status code ${statusCode}`);
      res.end();
    } catch (error) {
      console.log(error);
      res.writeHead(500, { "Content-Type": "text/plain" });
      res.end(" Server Error");
    }
  } else if (
    req.method === "GET" &&
    req.url.startsWith("/delay/") &&
    !isNaN(parseInt(req.url.split("/")[2]))
  ) {
    // Handle GET request for "/delay/{delay_in_seconds}"
    try {
      // Extract delay time from URL
      let delayInSeconds = parseInt(req.url.split("/")[2]);
      setTimeout(() => {
        res.writeHead(200, { "Content-Type": "text/plain" });
        res.end(`Delayed response after ${delayInSeconds} seconds`);
      }, delayInSeconds * 1000);
    } catch (error) {
      console.log(error);
      res.writeHead(500, { "Content-Type": "text/plain" });
      res.end(" Server Error");
    }
  } else {
    // Handle other requests (not "/html", "/json", "/uuid", "/status/{status_code}", or "/delay/{delay_in_seconds}")

    res.writeHead(404, { "Content-Type": "text/plain" });
    res.end("404 Not Found");
  }
});
// Start the server and listen on port 4000
server.listen(4000, () => {
  console.log("Server running on port 4000");
});
